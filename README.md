# Ejercicios de Python

En este repositorio se alojarán ejercicios que abarquen todo los capítulos de introducción a Python, excluyendo bibliotecas de ciencia de datos, PySpark y Flask.  En caso de dudas, consulte al instructor del curso.



## Ambiente de trabajo

Durante este curso, se le otorga un laptop personal con el intérprete 3.7 de Python instalado. Siga las indicaciones del instructor para cumplir con los ejercicios. 

## Formato de los ejercicios

Los ejercicios consistirán en una explicación breve del problema, un ejemplo de la entrada y de la salida deseada así como demás restricciones. En los casos requeridos, se dará un ejemplo o plantilla de código para comenzar a desarrollar la solución al ejercicio, de manera opcional.

Durante los ejercicios, se le indicará que escriba una función o programa de manera intercambiable. Para mayor comodidad, puede realizar todos los ejercicios como funciones y así mantenerlos en un único archivo `.py`.  

A no se que se le impongan restricciones, puede consultar funciones en línea que faciliten procesos que desea realizar en su programa. La biblioteca estándar de Python es bastante amplia y saber aprovechar sus funciones ya implementadas es beneficioso para cualquier programador. 

## Ejercicios

 1. Realice una función de fizz-buzz que reciba un número.  Si el número es divisible por tres, imprime *"Fuzz"*. Si es divisible por 5, devuelve *"Buzz"*. Si es divisible por 3 y 5, debería devolver *"FizzBuzz"*. Caso contrario, debería imprimir el mismo número.

2. Escriba una función que devuelva en una lista los múltiplos de 3 y 5 entre 0 y un límite **limit** parametrizado. Si el límite es 20, debería devolver 
 ```
 [3, 5, 6, 9, 10, 12, 15, 18, 20]
 ```  

 3. Escriba una función que reciba de input de consola, verifique que sea un *integer* y a partir de este número *n* genere un diccionario con tuplas  `{i:i*i}` siendo i los valores entre 0 y n. De modo tal que si el valor ingresado es 8, debería devolver:  `{1: 1, 2: 4, 3: 9, 4: 16, 5: 25, 6: 36, 7: 49, 8: 64}`

 4. Escriba una función que reciba de consola dos listas con sus elementos separados por coma, verifique  que las listas sean del mismo tamaño *n* y genere una lista de *n* tuplas donde la tupla del índice *i* sea `(first_list[i], second_list[i])`. Así, si ingresan las listas  `apple, orange, pineapple` y `1, 2, 3` se devuelva la lista `[('apple', 1), ('orange', 2), (pineapple, '3')]`.

5. Escriba una función que reciba una lista de valores numéricos separados por comas  de consola (Pueden ser integers o floats) y en otra línea dos valores *A* y *B* y siguiente la siguiente fórmula, siendo *D* cada elemento de la lista inicial:
Q = Raíz_Cuadrada(2CD/H)
  Devuelva una lista con el *Q* de cada *D*.

 6. Escriba una función que reciba dos números *X* y *Y* y devuelva una matriz  de *X* filas y *Y* columnas donde el elemento `i, j` sea el resultado de `i * j` . Así, si ingresan los valores `3 5` se debería de devolver `[[0, 0, 0, 0, 0], [0, 1, 2, 3, 4], [0, 2, 4, 6, 8]]`.

 7. Escriba una función que reciba de consola una lista de palabras separados por espacios e imprima la lista removiendo los elementos duplicados. Si el programa recibe: `hola mundo adios teclado python java hola lista duplicados teclado`, debería imprimir `hola mundo adios teclado python java lista duplicados`

8. Escriba un programa que reciba strings de números en base binaria e imprima una lista de booleanos del mismo tamaño donde el elemento del índice *i* indique si si el elemento del índice *i* de la lista original es divislbe por un parámetro *p* que sea *integer*. Así, pues si se recibe `['101', '100101', '1010']` y el parámetro *p* es `5`, se debería de devolver `[True, False, False]`. 

 9. Escriba un programa que reciba un string de un número de consola y valide que todo dígito presente sea divisible por un parámetro *p* enviado. Si lo son, devuelva la suma del producto de cada dígito por el parámetro *p*. 

 10. Escriba un programa que reciba una oración de consola y calcule el número de letras y el número de dígitos. Debe devolver estos valores en un diccionario de modo `{'Letras': 10, 'Digitos': 3}` para el input `hello world! 123`. 

 11. Escriba un programa que calcule el total de sumar un dígito  *d* con números compuestos de si mismo *n* veces.  Así, si *d* es `9`  y *n* es `4`, se debería sumar `d + dd + ddd + dddd` , dando un total de 11106. 

12. Escriba un programa que, **utilizando una lista de comprensión**,  eleve al cuadrado cada número impar de una lista en la que pueden haber valores de distintos tipos. La lista debe ser leída de consola con sus elementos separados por coma. Así, si ingresa el siguiente input `1,2,3,hello,4,5,6,7,world,23sd2,8,9`, entonces el output debería ser `[1, 9, 25, 49, 81]`.

 13. Escriba un programa que reciba una lista de contraseñas de consola separadas por comas e imprima únicamente las contraseñas que cumplan con las siguientes condiciones:
- Al menos una letra entre [a-z]
- Al menos una letra entre [A-Z]
- Al menos un dígito entre [1-9]
- Al menos un caracter de [$#@]
- Tamaño mínimo de 6 caracteres (exclusivo)
- Tamaño máximo de 12 caracteres (exclusivo)

 14.  Escriba un programa que calcule la frecuencia de la palabra ingresadas por consola. El output del programa debería devolver los valores ordenados de manera alfanumérica. De modo tal que si la entrada es: `New to Python or choosing between Python 2 and Python 3? Read Python 2 or Python 3.`, debería devolver:
 ```python
'2':2
'3.':1
'3?':1
'New':1
'Python':5
'Read':1
'and':1
'between':1
'choosing':1
'or':2
'to':1
```

15. Escriba un programa de Python que calcule, **de manera recursiva**, la suma de una lista de números separados por espacios que recibe de consola.

16. Escriba un programa de Python que imprima el número de día de los siguientes cinco días.

17. Escriba un programa de Python que encuentre la fecha del primer lunes de una semana *w* de un año *y*.  Así, si *w* es 50 y *y* es 2015, debería imprimir `Mon Dec 14 00:00:00 2015`.

18. Escriba un programa de Python que contenga dos clases llamadas `Rectangle` y `Circle`, donde la primera tenga atributos de `largo` y  `ancho` y un método que calcule su área, y la segunda debe tener un atributo de `radio`  y métodos que permitan calcular su perímetro y área.

19. Escriba un programa de Python que, **utilizando la expresión lambda**, elimine todos los números negativos de una lista de números.

